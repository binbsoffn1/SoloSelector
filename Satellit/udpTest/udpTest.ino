#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <string.h>

//*/
const char* ssid = "soloselector";
const char* password = "beitsound";
//*/
#define PORTNO 5000
WiFiUDP udp;


void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  Serial.println("HalliHallo");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(50);
    ESP.restart();
  }
  
  udp.begin(PORTNO);
  Serial.println("usp setup done");
  Serial.println(WiFi.localIP());
}


unsigned char packetBuffer[255];

void loop() {
  // put your main code here, to run repeatedly:
  int packetSize = udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(udp.remotePort());

    // read the packet into packetBufffer
    int len = udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    //String pString(&packetBuffer[0]);
    //switch commandString/Buffer here
    Serial.println("Contents:");
    Serial.println((char*)packetBuffer);

    // send a reply, to the IP address and port that sent us the packet we received
    udp.beginPacket(udp.remoteIP(), udp.remotePort());
    udp.write("k");
    udp.endPacket();
  }

}

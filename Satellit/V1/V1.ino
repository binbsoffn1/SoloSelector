//#include <HttpClient.h>
#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include "otaStuff.h"
#include "ioDEFS.h"
#include "helpers.h"
#include <NeoPixelBus.h>

//WIFI------RELATED
/*/
const char* ssid = "habibinetz";
const char* password = "32802302378759087053";
/*/


//*/
const char* ssid_ext = "soloselector_EXT";
const char* ssid = "soloselector";
const char* password = "beitsound";
//*/

MDNSResponder mdns;
ESP8266WebServer server(PORTNO);
WiFiUDP udp;


NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> strip(NUMPIX);


satState_t state;
int partySteps = 0;

void setup() {
  // put your setup code here, to run once:
	Serial.begin(115200);
	state.timeshift = 0;
	state.partyTime = 0;
	state.evalTime = 0;
	state.silentTime = 0;
	state.lightTime = 0;
	state.stroboTime = 0;
 state.stroboInterval = 0.1;
	strip.Begin();

	Serial.println("HalliHallo");
	WiFi.mode(WIFI_STA);

	/*
  char ssid[64];
  if(getRSSI(ssid_orig) > getRSSI(ssid_ext))
  {
    *ssid = *ssid_orig;
  }else{
    *ssid = *ssid_ext;
  }
  */
  Serial.println(ssid);
  //check wifi signal strength
	WiFi.begin(ssid, password);

  // Wait for connection
	while (WiFi.waitForConnectResult() != WL_CONNECTED) {
		Serial.println("Connection Failed! Rebooting...");
		delay(50);
		ESP.restart();
	}

	glow_connected();

  
	if (MDNS.begin(NAME))
	{
		Serial.print("my name is ");
		Serial.println(NAME);
	}
	udp.begin(PORTNO);
	//server.begin();
	delay(100);
	otaInit();
	MDNS.addService("http", "tcp", 80);

	//registerServerFunctions();

	Serial.println("Init completed");
	Serial.println(WiFi.localIP());

	state.action_last_triggered = 0;


	for (int i = 0; i < strip.PixelCount(); i++)
	{
		strip.SetPixelColor(i, 0);
	}
	strip.Show();

}

int _cpuTime = 0;

void loop() {
	ArduinoOTA.handle();
	//server.handleClient();
  handleUDPPackets();
  MDNS.update();
	//chekcconnectivity
	if (WiFi.isConnected() == false) {
		glow_disconnected();
		Serial.println("Connection Failed! Rebooting...");
		delay(500);
		ESP.restart();
	}
	//parametrierung kann man noch anpassen.....
	double realTime = getRealTime();
	if (realTime > state.stroboTime && state.stroboTime != 0)
	{
  
		state.evalTime = 0;
		if (realTime - state.action_last_triggered > state.stroboInterval)
		{
			state.action_last_triggered = realTime;
			strobo();
		}
	}
	else if (realTime > state.partyTime && state.partyTime != 0)
	{
  //stops evaluation and updates the party Animation.
  //PartyAnimation is highly dependend on the getREalTime Function...
  //Stepping through the paterns is done via this function...
		state.evalTime = 0;
		if (realTime - state.action_last_triggered > 0.05)
		{
			state.action_last_triggered = realTime;

			party(state.partyTime, partySteps, rosa);
			partySteps++;
		}
	}
	else if (realTime > state.silentTime && state.silentTime != 0)
	{
  //begin the animation for silencing the LED strip.
  //is actually the same pattern as seen on evaluation, only darkens LEDs bit by bit...
		state.evalTime = 0;
		if (realTime - state.action_last_triggered > 0.1)
		{
			state.action_last_triggered = realTime;
			silence(30);
		} 
		if (realTime - state.silentTime > 0.5)
		{
			//Switch this definitely off, even when Calls get slept over/ 
			//a race condition between a made call to evaluate and sleep time occurs. Especially seen on short evalTimes(1s and less)
			for(int i=0; i<strip.PixelCount(); i++){
				strip.SetPixelColor(i,RgbColor(0));
			}
			strip.Show();
			state.silentTime = 0;
		}
	}
	else if ((realTime > state.lightTime) && (state.lightTime != 0))
	{
		//After lighTime is reached, all LEDs are set to full output
		//a Call to this function is only made once.
		state.lightTime = 0;
		state.partyTime = 0;
		light();
		Serial.println("i light");
	}
	else if (realTime > state.evalTime && state.evalTime != 0)
	{
	   //Simple strand pattern
		//The glow/animation of the pattern is done on a 10fps refresh rate
		if (realTime - state.action_last_triggered > 0.1)
		{
			state.action_last_triggered = realTime;
			evaluate();
		}   
	}
}

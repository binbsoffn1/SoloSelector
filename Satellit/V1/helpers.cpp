#include <Arduino.h>
#include <NeoPixelBus.h>
#include <WiFiUdp.h>
#include "helpers.h"
#include "ioDEFS.h"


extern satState_t state;
extern NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> strip;
RgbColor Wheel(byte WheelPos);


extern RgbColor red(128, 0, 0);
extern RgbColor green(0, 128, 0);
extern RgbColor blue(0, 0, 128);
extern RgbColor white(128);
extern RgbColor black(0);
extern RgbColor rosa = RgbColor(100, 10, 70);

extern WiFiUDP udp;


int isPressed(int pin) {

	int cnt = 0;
	while (digitalRead(pin) == 0 && cnt < 500)
	{
		cnt++;
		delay(1);
	}
	return cnt > 100;
}

double getRealTime()
//return s actual time in seconds!
{
	return millis()/1000. + state.timeshift;
}


void glow_connected()
{
	for (int i = 0; i < 5; i++) {
		if (i < strip.PixelCount())
		{
			strip.SetPixelColor(i, RgbColor(0, 50 * i, 0));
		}
		if (9-i < strip.PixelCount())
		{
			strip.SetPixelColor(9-i, RgbColor(0, 50 * i, 0));
		}
	}

	for (int j = 0; j < strip.PixelCount(); j++)
	{
		strip.RotateRight(1);
		strip.Show();
		delay(20);
	}
}

void glow_disconnected()
{
	for (int i = 0; i < 5; i++) {
		if (i < strip.PixelCount())
		{
			strip.SetPixelColor(i, RgbColor(50*i, 0, 0));
		}
		if (9 - i < strip.PixelCount())
		{
			strip.SetPixelColor(9 - i, RgbColor( 50 * i, 0, 0));
		}
	}

	for (int j = 0; j < strip.PixelCount(); j++)
	{
		strip.RotateLeft(1);
		strip.Show();
		delay(20);
	}
}


void handleState()
{
	switch (state.state)
	{
	default:
	case not_connected:
			//glow reddish
		break;

	case waiting:
			//glow yellowish
		break;
	case ok:
		//glow greenish
		break;
	}
}

void strobo()
{
	static uint8_t step = 0;
	//call this @ refreshrate
	//fancy patterning
	step = step == 0;
	RgbColor clr = RgbColor(0, 0, 0);
	if (step)
	{
		clr = RgbColor(128, 128, 128);
	}
	for (int i = 0; i < strip.PixelCount(); i++)
	{
		strip.SetPixelColor(i, clr);
	}
	strip.Show();
}

void party(double start,int& step, RgbColor& c)
{
	//call this @ refreshrate
	//fancy patterning
	double timeZone = (int)(getRealTime() - start) % 300;//5 Minutes Repeating Cycles

	if (timeZone < 60) {
		rainbow(c,step);
	}
	else if (timeZone < 120)
	{
		theaterChase(c, step);
	}
	else if (timeZone < 180)
	{	
		rainbowCycle(step);
	}
	else if (timeZone < 240)
	{
		theaterChaseRainbow(step);
	}
	else if (timeZone < 300)
	{
		absoluteChaos(step);
	}
}

void silence(uint8_t darkVal)
{
	//all LEDs fade to off
	for (int i = 0; i < strip.PixelCount(); i++)
	{
		RgbColor clr;
		clr = strip.GetPixelColor(i);
		clr.Darken(darkVal);
		strip.SetPixelColor(i, clr);
	}
	evaluate();
}

//sets all pixels to be white, just need to call this once. No further animation is performed.
void light()
{
  RgbColor clr = RgbColor(80,80,80);
  for (int i = 0; i < strip.PixelCount(); i++)
  {
    strip.SetPixelColor(i,clr);
  }
  strip.Show();
}

void initforEval()
{
	
	for (int i = 0; i < NUMPIX; i++) {

		// pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
		strip.SetPixelColor(i, green); // Moderately bright green color.
	}
	int startidx = 2;
	for (int i = startidx; i < startidx + BRUSHWIDTH / 2; i++) {
		// pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
		int curidxofs = i - startidx + 1;
		strip.SetPixelColor(i%NUMPIX, RgbColor(16 * curidxofs / BRUSHWIDTH, 28 * curidxofs / BRUSHWIDTH, 202 * curidxofs / BRUSHWIDTH)); // Moderately bright green color.  
	}
	for (int i = (startidx + BRUSHWIDTH / 2); i < startidx + BRUSHWIDTH; i++) {
		// pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
		int curidxofs = BRUSHWIDTH - (i - startidx);
		strip.SetPixelColor(i%NUMPIX, RgbColor(16 * curidxofs / BRUSHWIDTH, 28 * curidxofs / BRUSHWIDTH, 202 * curidxofs / BRUSHWIDTH)); // Moderately bright green color.  

	}
}
void evaluate()
{
	//run eval	
	strip.RotateRight(1);
	strip.Show();
}


RgbColor Wheel(byte WheelPos) {
	WheelPos = 255 - WheelPos;
	if (WheelPos < 85) {
		return RgbColor(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if (WheelPos < 170) {
		WheelPos -= 85;
		return RgbColor(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return RgbColor(WheelPos * 3, 255 - WheelPos * 3, 0);
}




void colorWipe(RgbColor& c, uint8_t m_step) {
	uint8_t step = m_step / 2;	//slow down a little
	if (step % strip.PixelCount() == 0)//compute new color
	{
		c = RgbColor(Wheel(step * 6 % 255));
		//Serial.println(c.R);
	}


	strip.SetPixelColor(step%strip.PixelCount(), c);
	strip.Show();
}

void rainbow(RgbColor& c, uint8_t step) {
	for (int i = 0; i<strip.PixelCount(); i++) {
		strip.SetPixelColor(i, Wheel((i + step * 100 / NUMPIX) & 255));
	}
	strip.Show();
}

void rainbowCycle(uint8_t step) {
	for (int i = 0; i< strip.PixelCount(); i++) {
		strip.SetPixelColor(i, Wheel(((i * 256 / strip.PixelCount()) + step * 20) & 255));
	}
	strip.Show();
}

void theaterChase(RgbColor& c, uint8_t m_step)
{
	uint8_t step = m_step / 4;
	for (uint16_t i = 0; i < strip.PixelCount(); i++) {
		strip.SetPixelColor(i, 0);        //turn every  pixel off
	}

	for (uint16_t i = 0; i < strip.PixelCount(); i += 3) {
		strip.SetPixelColor((i + step) % strip.PixelCount(), c);    //turn every third pixel on
	}
	strip.Show();
}

void furiousColorBlink(RgbColor& c, uint8_t step)
{
	for (uint16_t i = 0; i < strip.PixelCount(); i++) {
		strip.SetPixelColor(i, 0);        //turn every  pixel off
	}

	for (uint16_t i = 0; i < strip.PixelCount(); i += 3) {
		strip.SetPixelColor((i + step) % strip.PixelCount(), c);    //turn every third pixel on
	}
	strip.Show();
}


void theaterChaseRainbow(uint8_t m_step)
{
	uint8_t step = m_step / 4;
	for (uint16_t i = 0; i < strip.PixelCount(); i++) {
		strip.SetPixelColor(i, 0);        //turn every  pixel off
	}


	for (int i = 0; i < strip.PixelCount(); i += 3) {
		strip.SetPixelColor((i + step) % strip.PixelCount(), Wheel((i + step) % 255));    //turn every third pixel on
	}
	strip.Show();
}



void absoluteChaos(uint8_t step)
{
	for (uint16_t i = 0; i < strip.PixelCount(); i++) {
		strip.SetPixelColor(i, 0);        //turn every  pixel off
	}


	for (int i = 0; i < strip.PixelCount(); i += 3) {
		strip.SetPixelColor((i + step) % strip.PixelCount(), Wheel((i + step) % 255));    //turn every third pixel on
	}
	strip.Show();
}
//	=========== Server Functions ===============================


void registerServerFunctions()
{
	//register HTTP Functions as lambda Functions...
	server.on("/", []() {
		String answer = "Ich bin  ";
		answer += NAME;
		answer += "\r\n";
		server.send(200, "text/html", answer);
		Serial.println(answer);
	});

	// toggle enable button:
	server.on("/setTime", []() {
		if (server.args() > 0)
		{
			for (int i = 0; i < server.args(); i++)
			{
				if (server.argName(i) == "timestamp")
				{
					double currentTime = strtod(server.arg(i).c_str(),0);
					state.timeshift = currentTime - millis()/1000;
				}
			}

		}
		else {
			state.timeshift = 0;
		}
		String answer = String(state.timeshift);
		answer += "\r\n";
		server.send(200, "text/html", answer);
	});

	server.on("/goParty", []() {

		if (server.args() > 0)
		{
			for (int i = 0; i < server.args(); i++)
			{
				if (server.argName(i) == "timestamp")
				{
					double currentTime = strtod(server.arg(i).c_str(), 0);
					state.partyTime = currentTime;
					state.silentTime = 0;
          state.lightTime = 0;
					state.evalTime = getRealTime();
				}
			}

		}
		else {
			state.partyTime = getRealTime();
			state.silentTime = 0;
			state.evalTime = 0;
			state.lightTime = 0;
		}
		String answer = String(state.partyTime);
		answer += "\r\n";
		server.send(200, "text/html", answer);
		//Serial.println("partytime: ");
		//Serial.println(state.partyTime);

		//server.send(200, "text/html", "goGogo");
		initforEval();
	});

	server.on("/goSilent", []() {
		if (server.args() > 0)
		{
			for (int i = 0; i < server.args(); i++)
			{
				if (server.argName(i) == "timestamp")
				{
					double currentTime = strtod(server.arg(i).c_str(), 0);
					state.silentTime = currentTime;
					state.evalTime = getRealTime();
					state.lightTime = 0;
					state.partyTime = 0;
				}
			}

		}
		else {
			state.silentTime = getRealTime();
			state.evalTime = 0;
			state.partyTime = 0;
			state.lightTime = 0;
		}
		String answer = String(state.silentTime);
		answer += "\r\n";
		server.send(200, "text/html", answer);
		initforEval();
	});

  server.on("/goLight", []() {
    if (server.args() > 0)
    {
      for (int i = 0; i < server.args(); i++)
      {
        if (server.argName(i) == "timestamp")
        {
          double currentTime = strtod(server.arg(i).c_str(), 0);
          state.lightTime = currentTime;
          state.partyTime = 0;
          state.evalTime = getRealTime();
          state.partyTime = 0;
        }
      }
    }
    else {
      state.silentTime = 0;
      state.lightTime = getRealTime();
      state.evalTime = 0;
      state.partyTime = 0;
    }
	String answer = String(state.silentTime);
	answer += "\r\n";
	server.send(200, "text/html", answer);
  });

}

int32_t getRSSI(const char* targetSSID)
{
  byte available_networks = WiFi.scanNetworks();
  for(int network = 0; network<available_networks; network++)
  {
    if(WiFi.SSID(network) == targetSSID)
    {
      return WiFi.RSSI(network);
    }
  }
  return 0;
}

void handleUDPPackets()
{
    char result = 'k';
    String strCMD;
    double timestamp = 0;
    double stroboInterval = 0;
    String pString;
    char packetBuffer[255];
  
    // read the packet into packetBufffer
    int packetSize = udp.parsePacket();
	if (packetSize) {
    int len = udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }
    pString = String(packetBuffer);
    //Serial.println(pString);
    //switch commandString/Buffer here
    //Serial.println("Contents:");
    //Serial.println(String(packetBuffer));
    //check for PageCMD
    uint8_t sStringIdx = pString.indexOf("timestamp");
    if(sStringIdx > 0)
    {
      uint8_t idx;
      idx = pString.indexOf(":", sStringIdx);
      timestamp = strtod(&pString[idx+1],0);//whoaa not that good, if passing multiple params...
    }
    
    sStringIdx = pString.indexOf("stroboInterval");
    Serial.println(sStringIdx);
  	if (sStringIdx > 0)
  	{
    	uint8_t idx;
    	idx = pString.indexOf(":", sStringIdx);
    	stroboInterval = strtod(&pString[idx + 1], 0);//whoaa not that good, if passing multiple params...
      Serial.println(stroboInterval);
    }
    
    //check for timestamp and vaue
    if(pString.startsWith("/setTime"))
    {
      Serial.println("setting Time");
        if(timestamp > 0)
        {
          state.timeshift = timestamp - millis()/1000;
        }else{
          state.timeshift = 0;
        }
        Serial.println(state.timeshift);
          
    }else if(pString.startsWith("/goSilent"))
    {
      Serial.println("going Silent");
      state.silentTime = getRealTime();
      state.partyTime = 0;
      state.evalTime = getRealTime();
      state.partyTime = 0;
      state.stroboTime = 0;
      if(timestamp > 0)
      {
        state.silentTime = timestamp;
      }
    }else if(pString.startsWith("/goParty"))
    {
      Serial.println("going Party");
      state.silentTime = 0;
      state.stroboTime = 0;
      state.evalTime = getRealTime();
      state.lightTime = 0;
      state.partyTime = getRealTime();
      if(timestamp > 0)
      {
        state.partyTime = timestamp;
      } 
    }
    else if (pString.startsWith("/goLight"))
    {
        Serial.println("going Light");
        state.silentTime = 0;
        state.evalTime = getRealTime();
        state.lightTime = getRealTime();
        state.stroboTime = 0;
        state.partyTime = 0;
        if (timestamp > 0)
        {
                state.lightTime = timestamp;
        }
    }
    else if (pString.startsWith("/goStrobo"))
    {
        Serial.println("going Strobo");
        state.silentTime = 0;
        state.evalTime = 0;
        state.lightTime = 0;
        if (timestamp != 0)
        {
            state.stroboTime = timestamp;
        }else{
            state.stroboTime = getRealTime();
        }
        state.partyTime = 0;
        if (stroboInterval > 0)
        {
            state.stroboInterval = stroboInterval;
        }else{
            state.stroboInterval = 0.3;
        }
    }
	else if (pString.startsWith("/setPattern"))
	{
		String sPattern;
		int idx;
		state.silentTime = 0;
		state.evalTime = 0;
		state.lightTime = 0;
		state.partyTime = 0;
    state.stroboTime = 0;
		String ptrn = "pattern:";
		idx = pString.lastIndexOf(ptrn) + ptrn.length();
		sPattern = String(&pString[idx]);
		for (int i = 0; i < sPattern.length()/3; i++)
		{
			RgbColor clr(sPattern[i], sPattern[i + 1], sPattern[i + 2]);
			strip.SetPixelColor(i, clr);
		}
		strip.Show();

    }else{
      result = 'n';
    }
	//Serial.println("received: ");
	//Serial.println(packetBuffer);
    // send a reply, to the IP address and port that sent us the packet we received
    udp.beginPacket(udp.remoteIP(), udp.remotePort());
    udp.write(result);
    udp.endPacket();
  }//end of lengthy packet
}



#pragma once
#include "helpers.h"
#include <ESP8266WebServer.h>

#ifndef __ioDEFS__h
#define __ioDEFS__h


//System Related
#define NAME "sophia"
//#define RESTART_PIN D2

//server related
#define PORTNO 5000
#define MAX_HP_LENGTH 100000	//max length of HTML Page in byte

//neopixelRelated
#define NUMPIX 100
#define PIX_PIN 2
#define BRUSHWIDTH 2

typedef struct state {
	double timeshift;
	double evalTime;
	double partyTime;
	double silentTime;
  double lightTime;
	double action_last_triggered;
	double stroboTime;
	double stroboInterval;
	uint8_t state;
}satState_t;

enum sEnum
{
	not_connected,
	waiting,
	ok,
};

extern ESP8266WebServer server;
extern satState_t state;


#endif //__ioDEFS__h

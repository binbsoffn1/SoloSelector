#ifndef __helpers__h
#define __helpers__h

#include <NeoPixelBus.h>

extern RgbColor red;
extern RgbColor green;
extern RgbColor blue;
extern RgbColor white;
extern RgbColor black;
extern RgbColor rosa;

//http server based approach to handle LED stripes
void registerServerFunctions();
//udp based approach to handling stripes, newly created function...
void handleUDPPackets();
int32_t getRSSI(const char* targetSSID);

int isPressed(int pin);
double getRealTime();

void handleState();
void strobo();
void party(double start, int& step, RgbColor& c);
void silence(uint8_t darkVal);
void light();
void evaluate();
void glow_connected();
void glow_disconnected();

void colorWipe(RgbColor& c, uint8_t step);
void rainbow(RgbColor& c, uint8_t step);
void rainbowCycle(uint8_t step);
void theaterChase(RgbColor& c, uint8_t step);
void theaterChaseRainbow(uint8_t m_step);

void furiousColorBlink(RgbColor& c, uint8_t step);
void absoluteChaos(uint8_t step);
#endif //__helpers__h

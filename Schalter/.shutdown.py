#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import os

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BOARD)

GPIO.setup(5,GPIO.IN,pull_up_down = GPIO.PUD_UP)

while True:
        time.sleep(0.5)
        if(GPIO.input(5) == False):
                os.system("sudo shutdown -h now")

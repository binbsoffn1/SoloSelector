#!/usr/bin/python3
import re
import socket
import RPi.GPIO as GPIO #need to install module yet.. using pip3 or python3-rpi.gpio
import time
import random
from threading import Thread
import netifaces as ni

DELAYTOACTION = 0.1
HTTP_TIMEOUT = 0.1
HTTP_RETRIES = 15
HTTP_PORT = 5000
SW_PIN = 8	#PHYS Pinnumbers.....
#maybe make this one  a dictionary to keep all relevant information like ipaddresses and latency together and in one place
ipPool = {}	#will contain ipaddresses of all connected devices
myIP = ""


LED_PIN = 7

MODE_0_PIN = 18
MODE_1_PIN = 22
MODE_2_PIN = 32
MODE_3_PIN = 36
MODE_4_PIN = 38
MODE_5_PIN = 40
MODE_ONE_TWO_KAI_PIN = MODE_1_PIN
pinList = [
MODE_0_PIN,
MODE_1_PIN,
MODE_2_PIN,
MODE_3_PIN,
MODE_4_PIN,
MODE_5_PIN
]

def timedDec(func):
	def wrapper(*args, **kw):
		now = time.time()
		result = func(*args,**kw)
		print(time.time()-now)
		return result
	return wrapper

###### base function #######
def goLight(ipaddress,tTime):
	global ipPool
	ipPool[ipaddress]["timestamp"] = tTime
	ipPool[ipaddress]["action"] = "/goLight"

def goParty(ipaddress,tTime):
	global ipPool
	ipPool[ipaddress]["timestamp"] = tTime
	ipPool[ipaddress]["action"] = "/goParty"

def goSilent(ipaddress,tTime):
	global ipPool
	ipPool[ipaddress]["timestamp"] = tTime
	ipPool[ipaddress]["action"] = "/goSilent"

def setTime(ipaddress,tTime):
	global ipPool
	ipPool[ipaddress]["timestamp"] = tTime
	ipPool[ipaddress]["action"] = "/setTime"

def sendToSatellite(sock, ipaddress, page, timestamp):
	retries = HTTP_RETRIES
	msg = str(page)
	if len(ipaddress) < 11:
		print("no ip")
		return 0
	if timestamp > 0:
		msg += "?timestamp:{:.2f}".format(timestamp)
		#print(msg)
	else: 
		print("timestamp: {}".format(timestamp))
	while retries:
		_now = time.time()
		timeout = False
		received = False
		while not timeout and not received:
			sock.sendto(bytes(msg, "utf-8"), (ipaddress, HTTP_PORT))
			timeout = (time.time()-_now) > HTTP_TIMEOUT
			time.sleep(0.01)#a short delay
			try:
				data,addr = sock.recvfrom(1024,socket.MSG_DONTWAIT)
				received = True
			except BlockingIOError:
				received = False
				pass
		if timeout:
			print("timeout"+str(retries))
			retries -= 1
		else:
			#print("got : {} \r\nfrom {}".format(data, addr[0]))
			#print("done sending to {}".format(ipaddress))
			retries = 0
	return received

				###started as own thread
def handleSatellite(ipaddress):
	global ipPool
	global kill
	ipPool[ipaddress]["active"] = True
	lastAct = "None"
	alive = True
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.bind(( '' ,HTTP_PORT + int( ipaddress.split( ".")[-1] )))
	try:
		while alive and kill == False:
			if lastAct != ipPool[ipaddress]["action"]:
				lastAct = ipPool[ipaddress]["action"]
				ts = ipPool[ipaddress]["timestamp"]
				if lastAct != "None":
					#print(lastAct)
					alive = sendToSatellite(sock, ipaddress, lastAct, ts) 
			time.sleep(HTTP_TIMEOUT)
	except Exception as e:
		print(e)
	print("alive >{}<".format(alive))
	del sock
	ipPool[ipaddress]["active"] = False
	return

############# SETSINGLE_PLAYER ############
def setSinglePlayer(target, partyTime):
	global ipPool
	for ip in ipPool:
		if ip == target:
			goParty(ip,partyTime)
		else:
			goSilent(ip,partyTime)

########## ONETWOKAI_MODE
kai = []
def initONETWOKAI(channel): ###Do this on a callback function on a falling edge on the KAI MODE SELECT PIN
	global kai
	kai = getRandomIP(ipPool)
	print(kai)

otk_ctr = 0
def oneTwoKai():
	global otk_ctr
	global lastSoloist
	global kai
	otk_ctr %= 3
	otk_ctr += 1
	if otk_ctr == 3:
		target = kai
	else:
		target = lastSoloist
		_now = time.time()
		if target == None:
			target = 0
		while ((target == lastSoloist) or (target == kai)) and (time.time()-_now < 0.2):
			target = getRandomIP(ipPool)
			time.sleep(0.1)
			if target == None:
				break
		lastSoloist = target
	if not target == None:
		setSinglePlayer(target, time.time()+DELAYTOACTION)
	else:
		return

'''Immer einer nach dem anderen ist dran. NAch der Runde wird der Pool der Spieler neu eingelesen.'''
########## nextExclusiveMode
npe_Pool = []
def nextExclusive():
	global npe_Pool
	global ipPool
	if len(npe_Pool) == 0:
		npe_Pool = [*ipPool].copy()
	try:
		target = npe_Pool[random.randint(0,len(npe_Pool)-1)]
		npe_Pool.remove(target)
		setSinglePlayer(target, time.time()+DELAYTOACTION)
	except ValueError:
		print("no next player available")

######### nextExclusiveWith PartyMode
obo_Pool = []
obo_ctr = 0

def nextExclusiveWithParty():
	global obo_Pool
	global ipPool
	global obo_ctr
	if len(obo_Pool) == 0:
		obo_Pool = [*ipPool].copy()
	obo_ctr = (obo_ctr == False)
	#print(obo_ctr)
	if obo_ctr == True:
		goAllParty()
	else:
		try:
			target = obo_Pool[random.randint(0,len(obo_Pool)-1)]
			obo_Pool.remove(target)
			setSinglePlayer(target,time.time()+DELAYTOACTION)
		except ValueError:
			print("no next player available")

############# singlePlayerWithLight ############	
def setSinglePlayerRestLight(target, partyTime):
	global ipPool
	for ip in ipPool:
		if ip == target:
			goParty(ip,partyTime)
		else:
			goLight(ip,partyTime)

########## lightAndExclusive Mode
'''wie one by one excluisive with party, nur dass alle nichtaktiven Leuchtstreidfen in Beleuchtungsmodeus versetzt werden'''
obol_Pool = []
ctr = 0
'''Ein Durchgang Licht, ein Durchgang Solo Mode'''
def nextExclusiveWithLight():
	global obol_Pool
	global ipPool
	global ctr
	if len(obol_Pool) == 0:
		obol_Pool = [*ipPool]
	ctr = (ctr == 0)
	if ctr == True:
		goAllLight()
	else:
		try:
			target = obol_Pool[random.randint(0,len(obol_Pool)-1)]
			obol_Pool.remove(target)
			setSinglePlayerRestLight(target,time.time()+DELAYTOACTION)
		except ValueError:
			print("no next player available")

############# ALLRANDOM_MODE
def getRandomIP(ipPool):
	try:
		return([*ipPool][ random.randrange(0,len(ipPool)) ])
	except ValueError:
		return None

#@timedDec
lastSoloist = []
def chooseNextSoloist():
	global ipPool
	global lastSoloist
	target = lastSoloist
	_now = time.time()
	while target == lastSoloist and time.time()-_now < 0.2:
		target = getRandomIP(ipPool)
	lastSoloist = target
	partyTime = time.time()+DELAYTOACTION
	if len(target):
		setSinglePlayer(target, partyTime)

############ ALL_SILENT_MODE
def goAllSilent():
	global ipPool
	for ip in ipPool:
		goSilent(ip,time.time())

############ ALL_PARTY_MODE
def goAllParty():
	global ipPool
	for ip in ipPool:
		goParty(ip, time.time())

############ ALL_LIGHT_MODE
def goAllLight():
	global ipPool
	for ip in ipPool:
		goLight(ip,time.time())

########## TIMESYNCER
#a function that should be called regularly to tell the servers they are still connected and not forgotten...
#@timedDec
def keepTimeSynced():
	global ipPool
	for ip in ipPool:
		ipPool[ip]["action"] = "None"
		time.sleep(0.1)#give it a little time to notify all Threads to resend TimeSync
		_now = time.time()
		setTime(ip,_now)

#not doing this asynchronously might result in large delays on execution
def updatePool():
	global ipPool
	initPool()
	alive = True
	#i = [*ipPool][0]
	for i in [*ipPool]:
		if ipPool[i]["active"] == False:#start a new Thread when Thread is inactive
			sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sock.bind(( '' ,HTTP_PORT + int(i.split( ".")[-1]) ))
			#clearing buffer
			try:
				while True:
					data,addr = sock.recvfrom(1024,socket.MSG_DONTWAIT)
			except:
				pass
			alive = sendToSatellite(sock, i, "hello", 0)
			if alive == True:
				print("alive");
				del sock
				t = Thread(target = handleSatellite, args =  (i,), daemon = True)
				t.start()
			else:
				del ipPool[i]


#t = Thread(target=  handleSatellite, args = (i,), daemon = True)
#t.start()
#kill = True
#ipPool[i]["action"] = "/setTime"
#ipaddress = i
#page = "/goParty"
#timestamp = 1.23

def initPool():
	global ipPool
	global myIP
	with open("/home/pi/dnsmasq.leases") as f:
		lines = f.readlines()
	for line in lines:
		if line.find("ESP") != -1 :
			ip = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",line)[0]
			if not ip in [*ipPool]:
				ipPool[ip] = {"active":False,"action":"None", "timestamp":0}
			#print(ip)


def poolUpdater():
	updatePool()
	keepTimeSynced()

############## keep it running !!!!!!! Functions
kill = False
def init():
	global myIP
	GPIO.setmode(GPIO.BOARD)
	GPIO.setwarnings(False)
	GPIO.setup(SW_PIN,GPIO.IN,pull_up_down=GPIO.PUD_UP)
	GPIO.setup(LED_PIN, GPIO.OUT,initial=1)
	for i in pinList:
		GPIO.setup(i,GPIO.IN, pull_up_down=GPIO.PUD_UP)
	#using this also works when someone else opens network,
	#in production i should use some faster methos,
	#as reading out the lease data
	myIP = ni.ifaddresses("wlan0")[ni.AF_INET][0]["addr"]
	random.seed()
	initPool()
	initONETWOKAI(0)
	#reInit Kai on every new selection of this mode
	GPIO.add_event_detect(MODE_ONE_TWO_KAI_PIN, GPIO.FALLING, callback = initONETWOKAI, bouncetime = 20)
	GPIO.add_event_detect(SW_PIN, GPIO.RISING, callback = buttonPressed, bouncetime = 20)

	
def buttonPressed(channel):
	global switchPressed
	switchPressed = True
#i = [*ipPool][0]
#kill = False
switchPressed = False
def run():
	global kill
	global switchPressed
	updateTimer = 0
	try:
		while True:
			_now = time.time()
			if _now - updateTimer > 3:
				updateTimer = _now
				poolUpdater()
			#if GPIO.input(SW_PIN) == 0:
			if switchPressed:
				switchPressed = False
				ctr = 0
				_tmpnow = _now
				while time.time()-_tmpnow < 0.3 or GPIO.input(SW_PIN) == 1:
					ctr += GPIO.input(SW_PIN) == 1
					time.sleep(0.01)
				#print(ctr)
				if ctr > 100:
					goAllSilent()
					print("none")
				elif ctr > 1:
					if GPIO.input(MODE_0_PIN) == 0:
						print("allPArty")
						goAllParty()
					if GPIO.input(MODE_ONE_TWO_KAI_PIN) == 0:
						print("next or Kai")
						oneTwoKai()
					if GPIO.input(MODE_2_PIN) == 0:
						print("next Exclusive")
						nextExclusive()
					#if GPIO.input(MODE_RANDOM_PIN) == 0:
					#	print("nextRandom")
					#	chooseNextSoloist()
					if GPIO.input(MODE_3_PIN) == 0:
						print("party or one")
						nextExclusiveWithParty()
					if GPIO.input(MODE_4_PIN) == 0:
						print("lightorExclusiveMode")
						nextExclusiveWithLight()
					if GPIO.input(MODE_5_PIN) == 0:
						print("Silence!!!")
						goAllSilent()
					for i in pinList:
						if GPIO.input(i) == False:
							print(i)
	except Exception as e:
		print(e)
		kill = True

if __name__ == "__main__":
	init()
	print("init done")
	for b in range(0,3):
		GPIO.output(LED_PIN,0)
		time.sleep(0.1)
		GPIO.output(LED_PIN,1)
		time.sleep(0.1)
	run()


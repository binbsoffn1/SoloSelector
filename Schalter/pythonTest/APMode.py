#!/usr/bin/python3
import os
import sys

if not "--AP" in sys.argv:
	#disable AP Services
	os.system("sudo systemctl disable hostapd")
	os.system("sudo systemctl disable dnsmasq")
	print("please reboot now")
	#disable fixed ip
else:
	os.system("sudo systemctl enable hostapd")
	os system("sudo systemctl enable dnsmasq")
	os.system("sudo systemctl start dnsmasq")
	os.system("sudo systemctl start hostapd")
	print("AP up and running probably")

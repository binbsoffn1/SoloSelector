#!/usr/bin/python3
from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
pinList = [29,31,33,35,37,16,40,38,36,32,22,18]
for i in pinList:
	GPIO.setup(i,GPIO.IN,pull_up_down=GPIO.PUD_UP)

while True:
	sleep(0.1)
	for i in pinList:
		if GPIO.input(i) == False:
			print(i)

#!/usr/bin/python3
import requests
import re
import socket
import RPi.GPIO as GPIO #need to install module yet.. using pip3 or python3-rpi.gpio
import time
import random
from threading import Thread
import netifaces as ni

DELAYTOACTION = 1.5
HTTP_TIMEOUT = 0.2
HTTP_RETRIES = 15
HTTP_PORT = 5000
SW_PIN = 8	#PHYS Pinnumbers.....
#maybe make this one  a dictionary to keep all relevant information like ipaddresses and latency together and in one place
#ipPool = []	#will contain ipaddresses of all connected devices
myIP = ""


LED_PIN = 7
MODE_0_PIN = 16   #set to 7 for debugging purposes
MODE_1_PIN = 18
MODE_2_PIN = 22
MODE_3_PIN = 32
MODE_4_PIN = 36
MODE_5_PIN = 38
MODE_6_PIN = 40
MODE_ONE_TWO_KAI_PIN = MODE_1_PIN
pinList = [
MODE_0_PIN,
MODE_1_PIN,
MODE_2_PIN,
MODE_3_PIN,
MODE_4_PIN,
MODE_5_PIN,
MODE_6_PIN
]

def timedDec(func):
	def wrapper(*args, **kw):
		now = time.time()
		result = func(*args,**kw)
		print(time.time()-now)
		return result
	return wrapper


###### base function #######
def setSinglePlayerRestLight(target, partyTime):
	global ipPool
	for ip in ipPool:
		if ip == target:
			goParty(ip,partyTime)
		else:
			goLight(ip,partyTime)

def setSinglePlayer(target, partyTime):
	global ipPool
	for ip in ipPool:
		if ip == target:
			goParty(ip,partyTime)
		else:
			goSilent(ip,partyTime)

def goLight(ipaddress,tTime):
	try:
		payload = {"timestamp":str(tTime)}
		sendToSatellite(ipaddress, "/goLight", payload)
	except ValueError:
		print("failure writing to {}".format(ipaddres))

def goParty(ipaddress,tTime):
	try:
		payload = {"timestamp":str(tTime)}
		sendToSatellite(ipaddress, "/goParty", payload)
	except ValueError:
		pass

def goSilent(ipaddress,tTime):
	try:
		payload = {"timestamp":str(tTime)}
		sendToSatellite(ipaddress, "/goSilent", payload)
	except ValueError:
		pass

def sendToSatellite(ipaddress,page,payload):
	sendToSatellite_base(ipaddress,page,payload)
#	t = Thread(target = sendToSatellite_base, args = (ipaddress,page,payload,))
#	t.start()


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("",HTTP_PORT))
#::Todo::
def clearRcvBuf(ipaddress):
	finished = False
	while not finished:
		try:
			data,addr = sock.recvfrom(1024,socket.MSG_DONTWAIT | socket.MSG_PEEK)
			if addr[0] == ipaddress:
				data,addr = sock.recvfrom(1024)
		except BlockingIOError:
			finished = True


def sendToSatellite_base(ipaddress, page, payload):
	global sock
	global ipPool
	if len(ipaddress) == 0:
		return
	retries = HTTP_RETRIES
	#msg = ipaddress + "/" + page
	msg = page
	for i in payload.keys():
		msg += "?"
		msg += i
		msg += ":"
		msg += str(payload[i])
	#print(msg)
	#try: #clear Buffer, an exception is thrown when no data is available
	_now = time.time()
	data = "just some dummy initializer data"
	while retries:
	#wait for answer
		_now = time.time()
		timeout = False
		received = False
		while not timeout and not received:
			sock.sendto(bytes(msg, "utf-8"), (ipaddress, HTTP_PORT))
			timeout = (time.time()-_now) > HTTP_TIMEOUT
			try:
				data,addr = sock.recvfrom(1024,socket.MSG_DONTWAIT | socket.MSG_PEEK)
				time.sleep(0.05)
				print(addr)
				if addr[0] == ipaddress:
					received = True
					data,addr = sock.recvfrom(1024)
			except BlockingIOError:
				#print("nocthing received")
				received = False
		if timeout:
			retries -= 1
			print(retries,ipaddress)
		else:
			retries = 0
	clearRcvBuf(ipaddress)

def sendToSatellite_base_http(ipaddress,page, payload):
	global ipPool
	retries = HTTP_RETRIES
	if len(ipaddress) == 0:
		return
	while retries:
		try:
			r = requests.post("http://"+ipaddress+":"+str(HTTP_PORT)+page, payload, timeout = HTTP_TIMEOUT)
			retries = 0
		except (requests. ReadTimeout,requests.ConnectTimeout,ConnectionError): #when ip is not responding remove it from pool#
			if retries == 1:
				ipPool.remove(ipaddress)
				print("timeout")
			retries -= 1



########## ONETWOKAI_MODE
kai = []
def initONETWOKAI(channel): ###Do this on a callback function on a falling edge on the KAI MODE SELECT PIN
	global kai
	kai = getRandomIP(ipPool)
	print(kai)


otk_ctr = 0
def oneTwoKai():
	global otk_ctr
	global lastSoloist
	global kai
	otk_ctr %= 3
	otk_ctr += 1
	if otk_ctr == 3:
		target = kai
	else:
		target = lastSoloist
		_now = time.time()
		if target == None:
			target = 0
		while target == lastSoloist or target == kai and time.time()-_now < 0.2:
			time.sleep(0.1)
			target = getRandomIP(ipPool)
			if target == None:
				break
		lastSoloist = target
	if not target == None:
		setSinglePlayer(target, time.time()+DELAYTOACTION)
	else:
		return




'''Immer einer nach dem anderen ist dran. NAch der Runde wird der Pool der Spieler neu eingelesen.'''
########## nextExclusiveMode

npe_Pool = []
def nextExclusive():
	global npe_Pool
	global ipPool
	if len(npe_Pool) == 0:
		npe_Pool = ipPool.copy()
	try:
		target = npe_Pool[random.randint(0,len(npe_Pool)-1)]
		npe_Pool.remove(target)
		#print(target)
		setSinglePlayer(target, time.time()+DELAYTOACTION)
	except ValueError:
		print("no next player available")



######### nextExclusiveWith PartyMode
obo_Pool = []
obo_ctr = 0
'''Ein Durchgang Party, ein Durchgang Solo Mode'''
def nextExclusiveWithParty():
	global obo_Pool
	global ipPool
	global obo_ctr
	if len(obo_Pool) == 0:
		obo_Pool = ipPool.copy()
	obo_ctr = (obo_ctr == False)
	print(obo_ctr)
	if obo_ctr == True:
		goAllParty()
	else:
		try:
			target = obo_Pool[random.randint(0,len(obo_Pool)-1)]
			obo_Pool.remove(target)
			setSinglePlayer(target,time.time()+DELAYTOACTION)
		except ValueError:
			print("no next player available")

########## lightAndExclusive Mode
'''wie one by one excluisive with party, nur dass alle nichtaktiven Leuchtstreidfen in Beleuchtungsmodeus versetzt werden'''
obol_Pool = []
ctr = 0
'''Ein Durchgang Licht, ein Durchgang Solo Mode'''
def nextExclusiveWithLight():
	global obol_Pool
	global ipPool
	global ctr
	if len(obol_Pool) == 0:
		obol_Pool = ipPool.copy()
	ctr = (ctr == 0)
	if ctr == True:
		goAllLight()
	else:
		try:
			target = obol_Pool[random.randint(0,len(obol_Pool)-1)]
			obol_Pool.remove(target)
			setSinglePlayerRestLight(target,time.time()+DELAYTOACTION)
		except ValueError:
			print("no next player available")

############# ALLRANDOM_MODE

def getRandomIP(ipPool):
	try:
		return(ipPool[ random.randrange(0,len(ipPool)) ])
	except ValueError:
		return None

#@timedDec
lastSoloist = []
def chooseNextSoloist():
	global ipPool
	global lastSoloist
	target = lastSoloist
	_now = time.time()
	while target == lastSoloist and time.time()-_now < 0.2:
		target = getRandomIP(ipPool)
	lastSoloist = target
	partyTime = time.time()+DELAYTOACTION
	if len(target):
		setSinglePlayer(target, partyTime)

############ ALL_SILENT_MODE
def goAllSilent():
	global ipPool
	for ip in ipPool:
		goSilent(ip,time.time())

############ ALL_PARTY_MODE
def goAllParty():
	global ipPool
	for ip in ipPool:
		goParty(ip, time.time())

############ ALL_LIGHT_MODE
def goAllLight():
	global ipPool
	for ip in ipPool:
		goLight(ip,time.time())






############## keep it running !!!!!!! Functions

def init():
	global myIP
	GPIO.setmode(GPIO.BOARD)
	GPIO.setwarnings(False)
	GPIO.setup(SW_PIN,GPIO.IN,pull_up_down=GPIO.PUD_UP)
	GPIO.setup(LED_PIN, GPIO.OUT,initial=1)
	for i in pinList:
		GPIO.setup(i,GPIO.IN, pull_up_down=GPIO.PUD_UP)
	#using this also works when someone else opens network,
	#in production i should use some faster methos,
	#as reading out the lease data
	myIP = ni.ifaddresses("wlan0")[ni.AF_INET][0]["addr"]
	random.seed()
	initPool()
	initONETWOKAI(0)
	#reInit Kai on every new selection of this mode
	GPIO.add_event_detect(MODE_ONE_TWO_KAI_PIN, GPIO.FALLING, callback = initONETWOKAI, bouncetime = 20)


#not doing this asynchronously might result in large delays on execution
def updatePool():
	global ipPool
	initPool()
	for i in ipPool:
		sock.sendto( bytes("updateing pool","utf-8") , (i,HTTP_PORT) )
		time.sleep(HTTP_TIMEOUT*2)
		try:
			data,addr = sock.recvfrom(1024,socket.MSG_DONTWAIT)
			#print(data)
		except BlockingIOError:
			ipPool.remove(i)
#	for i in ipPool:
#		print(i)

#@timedDec
ipPool = []
def initPool():
	global ipPool
	global myIP
	ipPool = []
	with open("/home/pi/dnsmasq.leases") as f:
		lines = f.readlines()
	for line in lines:
		if line.find("ESP") != -1 :
			ip = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",line)[0]
			ipPool.append(ip )
			print(ip)

#a function that should be called regularly to tell the servers they are still connected and not forgotten...
#@timedDec
def keepTimeSynced():
	global ipPool
	for ip in ipPool:
		_now = time.time()
		setTime(ip,_now)

#then there needs to be a possibility to send a timestamp
#sends lowest 32byte of current time in millis to satellites
def setTime(ipaddress,now):
	global ipPool
	payload = {"timestamp":now} # +ipPool[ipaddress]["latency"] 
	sendToSatellite(ipaddress, "/setTime", payload)




updateIP = 0
updateTimer = 0

def poolUpdater():
	while True:
		updatePool()
		keepTimeSynced()
		time.sleep(10)

if __name__ == "__main__":
	init()
	print("init done")
	t = Thread(target=poolUpdater,args=())
	t.start()
	for b in range(0,10):
		GPIO.output(LED_PIN,0)
		time.sleep(0.1)
		GPIO.output(LED_PIN,1)
		time.sleep(0.1)


	while True:
		_now = time.time()
		if GPIO.input(SW_PIN) == 0:
			ctr = 0
			_tmpnow = _now
			while time.time()-_tmpnow < 0.3 or GPIO.input(SW_PIN) == 1:
				ctr += GPIO.input(SW_PIN) == 1
				time.sleep(0.01)
			#print(ctr)
			if ctr > 100:
				goAllSilent()
				print("none")
			elif ctr > 1:
				if GPIO.input(MODE_0_PIN) == 0:
					print("allPArty")
					goAllParty()

				if GPIO.input(MODE_ONE_TWO_KAI_PIN) == 0:
					print("next or Kai")
					oneTwoKai()

				if GPIO.input(MODE_2_PIN) == 0:
					print("next Exclusive")
					nextExclusive()

				#if GPIO.input(MODE_RANDOM_PIN) == 0:
				#	print("nextRandom")
				#	chooseNextSoloist()

				if GPIO.input(MODE_3_PIN) == 0:
					print("party or one")
					nextExclusiveWithParty()

				if GPIO.input(MODE_4_PIN) == 0:
					print("lightorExclusiveMode")
					nextExclusiveWithLight()

				if GPIO.input(MODE_5_PIN) == 0:
					print("Silence!!!")
					goAllSilent()

				for i in pinList:
					if GPIO.input(i) == False:
						print(i)
